# Slash Full

## Running with NPM

1. **Configure MongoDB:**
   - Create a `.env` file in the `/server` directory with the following content:
     ```env
     # Database Configuration
     DB_LOCAL=false
     DB_HOST=<your_db_host_for_local_if_DB_LOCAL_true>
     DB_USER=<your_db_user>
     DB_PASSWORD=<your_db_password>
     DB_NAME=<your_db_name>
     DB_PORT=27017
     
     # Server Configuration
     SERVER_DOCKER_PORT=8080
     ```

2. **Install Dependencies and Start the Application:**
   - Navigate to the `/client` and `/server` directories separately and run the following commands:
     ```sh
     npm install
     npm run dev
     ```

## Running with Docker

### Configure Environment Variables

1. **Create a `.env` File:**
   - In the root of your project, create a `.env` file with the following content:
     ```env
     # MongoDB Configuration
     MONGODB_USER=<your_db_user>
     MONGODB_HOST=<your_db_host_for_local_if_DB_LOCAL_true>
     MONGODB_PASSWORD=<your_db_password>
     MONGODB_DATABASE=<your_db_name>
     MONGODB_LOCAL_PORT=7017
     MONGODB_DOCKER_PORT=27017
     
     # Server Configuration
     SERVER_LOCAL_PORT=6868
     SERVER_DOCKER_PORT=8080
     
     # Client Configuration
     CLIENT_ORIGIN=http://127.0.0.1:8888
     CLIENT_API_BASE_URL=http://127.0.0.1:6868
     
     CLIENT_LOCAL_PORT=8888
     CLIENT_DOCKER_PORT=80
     ```

### Start the Application

2. **Build and Start Services:**
   - Run the following command to build and start all services defined in your `docker-compose.yml` file:
     ```sh
     docker-compose up --build
     ```

3. **Stop the Application:**
   - To stop the application, press `Ctrl+C` in the terminal where `docker-compose` is running, or run:
     ```sh
     docker-compose down
     ```

