import React from "react";

import { Link } from "react-router-dom";
import HeaderSearch from "./HeaderSearch";
import LoginIcon from '@mui/icons-material/Login';
import LogoutIcon from '@mui/icons-material/Logout';
import { useSelector } from "react-redux";
import { logout, selectAuthData } from "../redux/auth/slice";
import Register from "../pages/Register";
import { useAppDispatch } from "../redux/store";

const Header = () => {
   const dispatch = useAppDispatch();

   const { token } = useSelector(selectAuthData);

   const handleLogout = () => {
      dispatch(logout());
   };

   return (
      <header className="header">
         <div className="header__wrapper">
            <Link to="/">
               <div
                  className="header__wrapper-logo-message"
               >
                  <div className="header-logo">
                     <svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M7.7381 0H7.44048H7.14286H6.84524H6.54762L0 25H0.595252H0.892857H1.19048H2.38097H2.97619H4.16667L10.7143 0H10.119H9.52381H8.92857H8.33333H7.7381Z" fill="#42F2F7"/>
                        <path d="M22.0238 0H21.7262H21.4286H21.1309H20.8333L14.2857 25H14.881H15.1786H15.4762H16.6667H17.2619H18.4524L25 0H24.4048H23.8095H23.2143H22.619H22.0238Z" fill="#42F2F7"/>
                     </svg>
                  </div>
                  <div className="header-message">
                     <p>Local-storage notes</p>
                  </div>
               </div>
            </Link>
            <div className="header__wrapper-search-actions">
               {
                  token ?
                  <>
                     <HeaderSearch />
                     <div className="header-actions">
                        <div className="header-button search--btn">
                           <svg
                              width="24"
                              height="24"
                              viewBox="0 0 24 24"
                              fill="none"
                              xmlns="http://www.w3.org/2000/svg"
                           >
                              <path
                                 fillRule="evenodd"
                                 clipRule="evenodd"
                                 d="M17.9757 10.2714C17.9757 12.3143 17.1642 14.2735 15.7197 15.718C14.2752 17.1625 12.316 17.974 10.2731 17.974C8.23026 17.974 6.27108 17.1625 4.82656 15.718C3.38204 14.2735 2.57052 12.3143 2.57052 10.2714C2.57052 8.22855 3.38204 6.26936 4.82656 4.82485C6.27108 3.38033 8.23026 2.56881 10.2731 2.56881C12.316 2.56881 14.2752 3.38033 15.7197 4.82485C17.1642 6.26936 17.9757 8.22855 17.9757 10.2714ZM16.5718 18.3866C14.5077 19.9891 11.9105 20.7447 9.30888 20.4996C6.70728 20.2544 4.29687 19.0271 2.56838 17.0673C0.839879 15.1075 -0.0767695 12.5627 0.00504028 9.95082C0.08685 7.33898 1.16097 4.85649 3.00873 3.00873C4.85649 1.16097 7.33898 0.08685 9.95082 0.00504028C12.5627 -0.0767695 15.1075 0.839879 17.0673 2.56838C19.0271 4.29687 20.2544 6.70728 20.4996 9.30888C20.7447 11.9105 19.9891 14.5077 18.3866 16.5718L23.5913 21.7765C23.7175 21.8941 23.8187 22.0358 23.8888 22.1933C23.959 22.3509 23.9968 22.5209 23.9998 22.6933C24.0028 22.8657 23.9711 23.037 23.9065 23.1968C23.842 23.3567 23.7458 23.502 23.6239 23.6239C23.502 23.7458 23.3567 23.842 23.1968 23.9065C23.037 23.9711 22.8657 24.0028 22.6933 23.9998C22.5209 23.9968 22.3509 23.959 22.1933 23.8888C22.0358 23.8187 21.8941 23.7175 21.7765 23.5913L16.5718 18.3866Z"
                                 fill="white"
                              />
                           </svg>
                        </div>
                        <Link to="/bin">
                           <div className="header-button bin--btn">
                              <svg
                                 width="19"
                                 height="22"
                                 viewBox="0 0 19 22"
                                 fill="none"
                                 xmlns="http://www.w3.org/2000/svg"
                              >
                                 <path
                                    d="M18.3214 1.35715H13.2321L12.8335 0.56407C12.749 0.394518 12.6189 0.251894 12.4579 0.152244C12.2968 0.0525943 12.1111 -0.000128418 11.9217 7.43575e-06H7.07411C6.88512 -0.000719083 6.69974 0.051807 6.53923 0.151568C6.37871 0.251329 6.24954 0.394289 6.16652 0.56407L5.76786 1.35715H0.678571C0.498603 1.35715 0.326006 1.42864 0.198749 1.5559C0.0714922 1.68316 0 1.85575 0 2.03572L0 3.39286C0 3.57283 0.0714922 3.74543 0.198749 3.87269C0.326006 3.99994 0.498603 4.07144 0.678571 4.07144H18.3214C18.5014 4.07144 18.674 3.99994 18.8013 3.87269C18.9285 3.74543 19 3.57283 19 3.39286V2.03572C19 1.85575 18.9285 1.68316 18.8013 1.5559C18.674 1.42864 18.5014 1.35715 18.3214 1.35715ZM2.25625 19.8058C2.28861 20.3226 2.51672 20.8077 2.89413 21.1623C3.27154 21.5168 3.76989 21.7142 4.28772 21.7143H14.7123C15.2301 21.7142 15.7285 21.5168 16.1059 21.1623C16.4833 20.8077 16.7114 20.3226 16.7437 19.8058L17.6429 5.42858H1.35714L2.25625 19.8058Z"
                                    fill="white"
                                 />
                              </svg>
                           </div>
                        </Link>
                        <div onClick={handleLogout} className="header-button">
                           <LogoutIcon />
                        </div>
                     </div>
                  </>
                  : <>
                     <Link to="/auth/login" >
                        <div className="header-button">
                           <LoginIcon />
                        </div>
                     </Link>
                  </>
               }
               

            </div>
         </div>
      </header>
   );
};

export default Header;
