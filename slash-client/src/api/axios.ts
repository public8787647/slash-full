import axios from "axios";

const instance = axios.create({
    baseURL: process.env.REACT_APP_API_BASE_URL || "http://localhost:8080",
    headers: {
        "Content-type": "application/json"
    }
});

instance.interceptors.request.use(config => {
    config.headers.Authorization = window.localStorage.getItem("token");
    return config;
});

export default instance;