import React from "react";
import { useSelector } from "react-redux";
import { useAppDispatch } from "../redux/store";
import { useNavigate } from "react-router-dom";

import "../scss/bin-page.scss";
import ErrorPage from "./ErrorPage";
import {
   Content,
   CategoriesTopBar,
   BackBtn,
   PermanentRemoveBtn,
} from "../components/";
import { selectData } from "../redux/data/selectors";
import { selectFilter } from "../redux/filter/selectors";
import { getData } from "../redux/data/base";
import { makeParse, makeUrl } from "../utils";
import { setBin } from "../redux/filter/slice";
import { selectAuthData } from "../redux/auth/slice";

function Bin() {
   const dispatch = useAppDispatch();

   const isSearch = React.useRef(false);
   const isMounted = React.useRef(false);

   const navigate = useNavigate();

   const { token } = useSelector(selectAuthData);
   const { error } = useSelector(selectData);
   const { category, sort, searchValue, urlParams } = useSelector(selectFilter);

   React.useEffect(() => {
      if (token && window.location.search) {
         makeParse(dispatch);
         isSearch.current = true;
      }
   }, [token, dispatch]);

   React.useEffect(() => {
      if (token) {
         if (!isSearch.current) {
            makeUrl({ sort, category, searchValue, bin: true, dispatch });
            dispatch(setBin(true));
            dispatch(getData());
         }
   
         isSearch.current = false;
      };
   }, [token, sort, category, searchValue, dispatch]);

   React.useEffect(() => {
      if (token) {
         if (isMounted.current && urlParams) {
            navigate(`?${urlParams}`);
         }
   
         isMounted.current = true;
      };
   }, [token, sort, category, searchValue, urlParams, navigate]);

   if (!token) {
      return (
         <ErrorPage errCode="Auth" message="Ligin or register to access notes" />
      );
   };

   if (error) {
      return <ErrorPage errCode="404" message={`Redux problem: ${error}`} />;
   }

   return (
      <div className="main__wrapper-bin">
         <div className="top-menu">
            <BackBtn />
            <PermanentRemoveBtn />
            <CategoriesTopBar />
         </div>
         <Content noteType={"bin"} />
      </div>
   );
}

export default Bin;
