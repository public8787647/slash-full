import React from "react";
import { useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";
import { useAppDispatch } from "../redux/store";
import "../scss/main.scss";
import {
   Content,
   AddNoteBtn,
   SortTopBar,
   CategoriesTopBar,
} from "../components";
import ErrorPage from "./ErrorPage";
import { selectData } from "../redux/data/selectors";
import { selectFilter } from "../redux/filter/selectors";
import { setBin } from "../redux/filter/slice";
import { getData } from "../redux/data/base";
import { makeParse, makeUrl } from "../utils";
import { selectAuthData } from "../redux/auth/slice";

const Home = () => {

   // =======================================================================
   // Redux
   // =======================================================================

   const dispatch = useAppDispatch();

   const { error } = useSelector(selectData);

   const { token } = useSelector(selectAuthData);
   const { category, sort, searchValue, urlParams } = useSelector(selectFilter);

   // =======================================================================
   // Navigate and URL parsing
   // =======================================================================

   const navigate = useNavigate();

   const isSearch = React.useRef(false);
   const isMounted = React.useRef(false);

   React.useEffect(() => {
      if (token && window.location.search) {
         makeParse(dispatch);
         isSearch.current = true;
      }
   }, [token, dispatch]);

   React.useEffect(() => {
      if (token) {
         if (!isSearch.current) {
            makeUrl({ sort, category, searchValue, bin: false, dispatch });
            dispatch(setBin(false));
            dispatch(getData());
         }
         isSearch.current = false;
      };
   }, [token, sort, category, searchValue, dispatch]);

   React.useEffect(() => {
      if (token) {
         if (isMounted.current && urlParams) {
            navigate(`?${urlParams}`);
         }
   
         isMounted.current = true;
      };
   }, [token, sort, category, searchValue, urlParams, navigate]);

   if (!token) {
      return (
         <ErrorPage errCode="Auth" message="Ligin or register to access notes" />
      );
   };

   if (error) {
      return <ErrorPage errCode={"404"} message={`${error}`} />;
   }

   return (
      <div className="main__wrapper" data-testid="main__wrapper">
         <div className="topbar" data-testid="topbar">
            <div className="topbar__wrapper-add-note-sort">
               <AddNoteBtn />
               <SortTopBar />
            </div>
            <CategoriesTopBar />
         </div>
         <Content />
      </div>
   );
};

export default Home;
