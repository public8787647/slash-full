import React from "react";
import "../scss/form.scss";
import { Paper, Button, TextField } from "@mui/material";
import { useForm } from "react-hook-form";
import { useAppDispatch } from "../redux/store";
import { authRegister, selectAuthData } from "../redux/auth/slice";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";

const Register = () => {
    const dispatch = useAppDispatch();
    const navigate = useNavigate();

    const { token, error } = useSelector(selectAuthData);
    
    const {register, handleSubmit, setError, formState: {errors, isValid}} = useForm({
        defaultValues: {
            name: "",
            email: "",
            password: "",
        },
        mode: "onChange",
    });

    const handleErrors = React.useCallback((error: string) => {
        setError("password", {
            message: error || "",
        });
    }, []);

    const onSubmit = async (values: {name: string, email: string; password: string}) => {
        dispatch(authRegister(values));
        handleErrors(error);
    };

    React.useEffect(() => {
        if (token) {
            window.localStorage.setItem("token", token);
            navigate(`/`);
        };

    }, [token, navigate]);

    React.useEffect(() => {
        handleErrors(error);

    }, [error, handleErrors]);

   return (
    <div className="form-page">
        <form onSubmit={handleSubmit(onSubmit)}>
            <Paper className="form-page-paper">
                <TextField
                    className="form-page-textfield"
                    label="<Name>"
                    type="name"
                    fullWidth
                    helperText={errors.email?.message}
                    {...register("name")}
                />
                <TextField
                    className="form-page-textfield"
                    label="<Email>"
                    type="email"
                    fullWidth
                    helperText={errors.email?.message}
                    {...register("email", {required: "Enter a correct email"})}
                />
                <TextField
                    className="form-page-textfield"
                    label="<Password>"
                    type="password"
                    fullWidth
                    helperText={errors.password?.message}
                    {...register("password", {required: "Enter a correct password"})}
                />
                <Button
                    size="large"
                    type="submit"
                    fullWidth
                >Register</Button>
            </Paper>
        </form>
    </div>
   );
};

export default Register;
