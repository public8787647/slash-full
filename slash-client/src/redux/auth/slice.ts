import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { AuthSliceData, Status } from "../../types";
import axios from "../../api/axios";
import { handleErrors } from "../data/utils";
import { RootState } from "../store";

export const initialState: AuthSliceData = {
    token: window.localStorage.getItem("token") || null,
    status: Status.IDLE,
    error: "",
};

export const authRegister = createAsyncThunk<string, {name: string, email: string, password: string}, {state: RootState}>(
    "auth/register",
    async ({name, email, password}, {rejectWithValue}) => {
        try {
            const postObj = {
                name,
                email,
                pwdHash: password,
            };
            const { data } = await axios.post("/auth/register", postObj);
            return data.token;
        } catch (error: any) {
            return handleErrors(error, rejectWithValue, "Error while register a user");
        };
    },
);

export const authLogin = createAsyncThunk<string, {name: string, email: string, password: string}, {state: RootState}>(
    "auth/login",
    async ({name, email, password}, {rejectWithValue}) => {
        try {
            const postObj = {
                name,
                email,
                pwdHash: password,
            };
            const { data } = await axios.post("/auth/login", postObj);
            return data.token;
        } catch (error: any) {
            return handleErrors(error, rejectWithValue, "Error while register a user");
        };
    },
);

const authSlice = createSlice({
    name: "auth",
    initialState,
    reducers: {
        logout: (state) => {
            window.localStorage.removeItem("token");
            state.token = null;
        },
    },
    extraReducers: (builder) => {
        builder
        .addCase(authRegister.pending, (state) => {
            state.token = null;
            state.status = Status.LOADING;
        })
        .addCase(authRegister.fulfilled, (state, action) => {
            state.token = action.payload;
            state.status = Status.SUCCESS;
        })
        .addCase(authRegister.rejected, (state) => {
            state.token = null;
            state.status = Status.ERROR;
            state.error = "Failed to register";
        });

        builder
        .addCase(authLogin.pending, (state) => {
            state.token = null;
            state.status = Status.LOADING;
        })
        .addCase(authLogin.fulfilled, (state, action) => {
            state.token = action.payload;
            state.status = Status.SUCCESS;
        })
        .addCase(authLogin.rejected, (state) => {
            state.token = null;
            state.status = Status.ERROR;
            state.error =  "Failed to login";
        });
    }
});

export const selectAuthData = (state: RootState) => state.auth;

export const { logout } = authSlice.actions;

export default authSlice.reducer;