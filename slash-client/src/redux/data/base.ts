import { createAsyncThunk, nanoid } from "@reduxjs/toolkit";

import { AppDispatch, RootState } from "../store";
import { NoteType, ServerNoteType, ServerTagType, TagType } from "../../types";
import { getDemonstrationData, handleErrors, helperNoteObj, helperTagObj } from "./utils";
import axios from "../../api/axios";
import { setActiveCategory } from "../filter/slice";
import moment from "moment";

const dateFormat = "MM-DD-YYYY HH:mm";

// Fetching data

export const getData = createAsyncThunk<[NoteType[], TagType[]], void, { state: RootState }>(
    "data/getData",
    async (_, { getState, rejectWithValue }) => {
        let data, notes: NoteType[], tags: TagType[];
        //const { category, sort, searchValue, bin } = getState().filter;
        const urlParams = getState().filter.urlParams;
        // const reverseOrder = sort.sortProp2 === "asc" ? false : true;
        // const orderByCreated = sort.sortProp1 === "created" ? true : false;
        // const params = {
        //     orderByCreated,
        //     reverseOrder,
        //     bin,
        //     tagName: category,
        //     textSearch: searchValue
        // };
        // const dbExistance = await checkDbExistance();
        // if (dbExistance) {
            try {
                [ {data: notes}, {data: tags} ] = await Promise.all([
                    axios.get<NoteType[]>(`/notes?${urlParams}`),
                    axios.get<TagType[]>("/tags")
                ]);
            } catch (error: any) {
                return handleErrors(error, rejectWithValue, "Error while getting data");
            };
        // } else {
        //     [notes, tags] = getDemonstrationData();
        //     await db.setData({ notes, tags });
        //};
        return [notes, tags];
    },
);

export const getTags = createAsyncThunk<TagType[], void, { state: RootState }>(
    "data/getTags",
    async (_, { rejectWithValue }) => {
        try {
            const { data: tags } = await axios.get<TagType[]>("/tags");
            return tags;
        } catch (error: any) {
            return handleErrors(error, rejectWithValue, "Error while getting tags");
        };
    },
);

// Notes CRUD

export const postNote = createAsyncThunk<NoteType, void, { state: RootState }>(
    "data/postNote",
    async (_, { dispatch, rejectWithValue }) => {
        try {
            const note: ServerNoteType = {
                text: "",
                tags: [],
                created: moment().format(dateFormat),
                edited: "",
                bin: false,
            };
            await axios.post("/notes", note);
            //await dispatch(getNote(id));
            await dispatch(getData());
        } catch (error: any) {
            return handleErrors(error, rejectWithValue, "Error while posting a note");
        };
    },
);

export const getNote = createAsyncThunk<NoteType, string, { state: RootState }>(
    "data/getNote",
    async (id, { dispatch, rejectWithValue }) => {
        try {
            await dispatch(getTags());
            const { data: note } = await axios.get<NoteType>(`/notes/${id}`);
            return note;
        } catch (error: any) {
            return handleErrors(error, rejectWithValue, "Error getting a note");
        };
    },
);

export const patchNote = createAsyncThunk<NoteType, {id: string, text?: string, addTagId?: string, deleteTagId?: string, bin?: boolean, tagName?: string}, { state: RootState }>(
    "data/patchNote",
    async ({ id, text, addTagId, deleteTagId, bin, tagName }, { dispatch, rejectWithValue, getState }) => {
        const { notes, selectedNote } = getState().data;
        let note;
        if (selectedNote?.id === id) {
            note = selectedNote;
        } else {
            note = notes.find((item: NoteType) => item.id === id);
        };
        if (!note) {
            throw new Error(`No note found with id: ${id}`)
        }
        const updated = helperNoteObj(note, { text, addTagId, deleteTagId, bin })
        try {
            await axios.patch(`/notes/${id}`, {
                edited: moment().format(dateFormat),
                text: updated.text,
                tags: updated.tags,
                bin: updated.bin,
                tagName
            });
            if (notes.length > 0) {
                await dispatch(getData());
            } else {
                await dispatch(getNote(id));
            };
        } catch (error: any) {
            return handleErrors(error, rejectWithValue, "Error while patching a note");
        };
    },
);

export const deleteNote = createAsyncThunk<string, string, { state: RootState }>(
    "data/deleteNote",
    async (id, { rejectWithValue }) => {
        try {
            await axios.delete(`/notes/${id}`);
            return id;
        } catch (error: any) {
            return handleErrors(error, rejectWithValue, "Error while deleting a note");
        };
    },
);

export const deleteNotes = createAsyncThunk<void, void, { state: RootState }>(
    "data/deleteNotes",
    async (_, { rejectWithValue, getState }) => {
        const { notes } = getState().data;
        const filterBinNotes = notes.filter(item => item.bin === true);
        const ids = filterBinNotes.map(item => item.id);
        try {
            await axios.get("/notes",);
            // await axios.delete("/notes", data: {
            //     ids
            // });
        } catch (error: any) {
            return handleErrors(error, rejectWithValue, "Error while deleting a note");
        };
    },
);

// Tags CRUD

export const postTag = createAsyncThunk<void, {tagName: string, noteId?: string}, { state: RootState }>(
    "data/postTag",
    async ({ tagName, noteId }, { dispatch, rejectWithValue, getState }) => {
        const { notes } = getState().data;
        try {
            const tag: ServerTagType = {
                name: tagName,
                created: moment().format(dateFormat),
                notes: []
            }
            await axios.post("/tags", tag);
            if (notes.length > 0) {
                await dispatch(getData());
            } else {
                if (noteId) {
                    await dispatch(getTags());
                    await dispatch(getNote(noteId));
                }
            }
        } catch (error: any) {
            return handleErrors(error, rejectWithValue, "Error while posting a tag");
        };
    }
);

export const getTag = (tagId: string) => (dispatch: AppDispatch, getState: () => RootState): TagType => {
    const { tags } = getState().data;
    const res = tags.find((item: TagType) => item.id === tagId);
    if (res) {
       return res;
    } else {
       throw new Error(`No tag found with id: ${tagId}`);
    };
};

export const patchTag = createAsyncThunk<void, { id: string, name?: string, addNoteId?: string, deleteNoteId?: string }, { state: RootState }>(
    "data/patchTag",
    async ({ id, name, addNoteId, deleteNoteId }, { dispatch, getState, rejectWithValue }) => {
        const { tags } = getState().data;
        const tag = tags.find(item => item.id === id);
        if (!tag) {
            throw new Error(`No tag found with id: ${id}`)
        }
        const updated = helperTagObj(tag, { name, addNoteId, deleteNoteId });
        try {
            await axios.patch(`/tags/${id}`, {
                name: updated.name,
                notes: updated.notes

            });
            await dispatch(getTags());
        } catch (error: any) {
            return handleErrors(error, rejectWithValue, "Error while patching a tag");
        };
    },
);

export const deleteTag = createAsyncThunk<void, { id: string, noteId?: string }, { state: RootState }>(
    "data/deleteTag",
    async ({ id, noteId }, { dispatch, getState, rejectWithValue }) => {
        const { notes } = getState().data;
        try {
            if (noteId) {
                await dispatch(patchNote({ id: noteId, deleteTagId: id }));
            } else {
                await axios.delete(`/tags/${id}`);
                dispatch(setActiveCategory(null));
            };
            if (notes.length > 0) {
                await dispatch(getData());
            } else if (noteId) {
                await dispatch(getNote(noteId));
            }
        } catch (error: any) {
            return handleErrors(error, rejectWithValue, "Error while deleting a tag");
        };
    }
);