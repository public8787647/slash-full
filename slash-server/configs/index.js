import "dotenv/config";
export * from "./db.config.js";

export const CORS_CONFIG = {
    origin: process.env.CLIENT_ORIGIN || "http://localhost:3000",
    optionsSuccessStatus: 200,
};

export const PORT = process.env.SERVER_DOCKER_PORT || "8080";