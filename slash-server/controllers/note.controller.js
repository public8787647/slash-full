import Note from "../models/note.db.js";
import User from "../models/user.db.js";
import Tag from "../models/tag.db.js";

const returnNoteObj = {
    _id: 1,
    text: 1,
    created: 1,
    edited: 1,
    tags: 1,
    bin: 1
}

export const createNote = async (req, res) => {
    try {
        const user = await User.findOne(
            {
                _id: req.userId,
            }
        );

        if (!user) {
            return res.status(404).json({ message: `User not found with id: ${req.userId}` });
        };

        const note = new Note({
            ...req.body,
            author: user._id,
        });
        await note.save();
        user.notes.push(note._id);
        await user.save();

        res.status(201).json(note);
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: `Problem to create a note: ${error.message}`
        });
    };
};

export const getNotes = async (req, res) => {
    try {
        const queryParams = new URLSearchParams(req.query);
        const sort = queryParams.get("_sort");
        const order = queryParams.get("_order");
        const tagsLike = queryParams.get("tags_like");
        const textLike = queryParams.get("text_like");
        const bin = queryParams.get("bin");

        const query = {};

        if (tagsLike) {
            const tag = await Tag.findOne({ name: tagsLike });
            query.tags = tag._id;
        };

        if (textLike) {
            query.text = { $regex: textLike, $options: "i" }; // Case-insensitive regex search
        };

        // Prepare the sort object
        const sortObj = {};
        if (sort && order) {
            sortObj[sort] = order === "asc" ? 1 : -1;
        };

        if (bin) {
            query.bin = bin === "true" ? true : false;
        };

        const user = await User.findOne(
            {
                _id: req.userId,
            }
        );

        if (!user) {
            return res.status(404).json({ message: `User not found with id: ${req.userId}` });
        };

        const notes = await Note.find({ author: user._id, ...query }, returnNoteObj).sort(sortObj).lean();

        notes.forEach(note => {
            note.id = note._id;
            delete note._id;
        });

        res.status(200).json(notes);
        
    } catch (err) {
        console.log(err);
        res.status(500).json({
            message: 'Problem to get notes'
        });
    };
};

export const getNote = async (req, res) => {
    try {
        const note = await Note.findOne({ _id: req.params.id, author: req.userId }).lean();
        //const note = await Note.findOne({ _id: req.params.id }).lean();
        if (!note) {
            return res.status(404).json({ message: `Note not found with id: ${req.params.id}` });
        };

        note.id = note._id;
        delete note._id;

        res.status(200).json(note);
    } catch (err) {
        console.log(err);
        res.status(500).json({
            message: 'Problem to get a note'
        });
    };
};

export const deleteNote = async (req, res) => {
    try {
        const note = await Note.findOneAndDelete({ _id: req.params.id, author: req.userId });
        //const note = await Note.findOneAndDelete({ _id: req.params.id });
        if (!note) {
            return res.status(404).json({ message: `Note not found with id: ${req.params.id}` });
        }

        await User.updateMany({ notes: req.params.id }, { $pull: { notes: req.params.id } });
        await Tag.updateMany({ notes: req.params.id }, { $pull: { notes: req.params.id } });

        note.id = note._id;
        delete note._id;

        res.status(200).json({ message: `Note deleted successfully`, note });
    } catch (err) {
        console.log(err);
        res.status(500).json({
            message: 'Problem to delete a note'
        });
    };
};

export const updateNote = async (req, res) => {
    try {
        const id = req.params.id;
        let tags = req.body.tags;
        const tagName = req.body.tagName;

        let note = await Note.findOne({ _id: id, author: req.userId });
        //let note = await Note.findOne({ _id: id });
        if (!note) {
            return res.status(404).json({ message: `Note not found with id: ${id}` });
        };

        if (req.body.tags) {
            const updatedTags = req.body.tags;
            const currentTags = note.tags;

            const addedTags = updatedTags.filter((tag) => !currentTags.includes(tag));
            const removedTags = currentTags.filter((tag) => !updatedTags.includes(tag));

            if (addedTags.length > 0) {
                await Tag.updateMany(
                {
                    _id: {
                    $in: addedTags,
                    },
                },
                {
                    $addToSet: {
                    notes: id,
                    },
                }
                );
            };

            if (removedTags.length > 0) {
                await Tag.updateMany(
                {
                    _id: {
                    $in: removedTags,
                    },
                },
                {
                    $pull: {
                    notes: id,
                    },
                }
                );
            };
        };

        if (req.body.tagName) {
            const tag = await Tag.findOne({ name: tagName });
            tags = !tags ? note.tags : tags;
            if (tag) {
                const foundTag = note.tags.includes(tag.id);
                if (foundTag) {
                    return res.status(400).json({ message: `The tag ${tagName} already included in a note` });
                };
                tag.notes.push(id);
                await tag.save();
                tags.push(tag.id);
            } else {
                const tag = new Tag({
                    name: tagName,
                    notes: [id],
                    created: req.body.edited,
                    author: req.userId
                });
                await tag.save();

                tags.push(tag.id);
            };
        };

        const updatedNote = await Note.findOneAndUpdate(
            { _id: id },
            {
                $set: {
                    text: req.body.text,
                    tags: tags,
                    edited: req.body.edited,
                    bin: req.body.bin
                }
            },
            { new: true }
        );

        updatedNote.id = updatedNote._id;
        delete updatedNote._id;

        res.json({
            message: `Updated note, id: ${note._id}`,
            note: updatedNote
        });
    } catch (err) {
        console.log(err);
        res.status(500).json({
            message: 'Problem to update a note'
        });
    };
};