import Tag from "../models/tag.db.js";
import User from "../models/user.db.js";
import Note from "../models/note.db.js";

const returnTagObj = {
    _id: 1,
    name: 1,
    created: 1,
    notes: 1
}

export const createTag = async (req, res) => {
    try {
        const user = await User.findOne(
            {
                _id: req.userId,
            }
        );
        if (!user) {
            return res.status(404).json({ message: `User not found with id: ${req.userId}` });
        };

        const tag = new Tag({
            ...req.body,
            author: user._id,
        });
        await tag.save();
        user.tags.push(tag._id);
        await user.save();

        tag.id = tag._id;
        delete tag._id;

        res.status(201).json(tag);
    } catch (err) {
        console.log(err);
        res.status(500).json({
            message: 'Problem to create a tag'
        });
    };
};

export const getTags = async (req, res) => {
    try {
        // const tags = await Tag.find(
        //     {},
        //     { ...returnTagObj }
        // ).lean();

        const user = await User.findOne(
            {
                _id: req.userId,
            }
        );
        if (!user) {
            return res.status(404).json({ message: `User not found with id: ${req.userId}` });
        };

        const tags = await Tag.find({ author: user._id }, { ...returnTagObj }).lean();

        tags.forEach(tag => {
            tag.id = tag._id;
            delete tag._id;
        });

        res.status(200).json(tags);
    } catch (err) {
        console.log(err);
        res.status(500).json({
            message: 'Problem to get tags'
        });
    };
};

export const getTag = async (req, res) => {
    try {
        const tag = await Tag.findOne({ _id: req.params.id, author: req.userId });
        //const tag = await Tag.findOne({ _id: req.params.id });
        if (!tag) {
            return res.status(404).json({ message: `Tag not found with id: ${req.params.id}` });
        };

        tag.id = tag._id;
        delete tag._id;

        res.status(200).json(tag);
    } catch (err) {
        console.log(err);
        res.status(500).json({
            message: 'Problem to get a tag'
        });
    };
};

export const deleteTag = async (req, res) => {
    try {
        const tag = await Tag.findOneAndDelete({ _id: req.params.id, author: req.userId });
        //const tag = await Tag.findOneAndDelete({ _id: req.params.id });
        if (!tag) {
            return res.status(404).json({ message: `Tag not found with id: ${req.params.id}` });
        }

        await User.updateMany({ tags: req.params.id }, { $pull: { tags: req.params.id } });
        await Note.updateMany({ tags: req.params.id }, { $pull: { tags: req.params.id } });

        tag.id = tag._id;
        delete tag._id;

        res.status(200).json({ message: `Tag deleted successfully`, tag });
    } catch (err) {
        console.log(err);
        res.status(500).json({
            message: 'Problem to delete a tag'
        });
    };
};

export const updateTag = async (req, res) => {
    try {
        const id = req.params.id;

        let tag = await Tag.findOne({ _id: id, author: req.userId });
        //let tag = await Tag.findOne({ _id: id });
        if (!tag) {
            return res.status(404).json({ message: `Tag not found with id: ${id}` });
        };

        if (req.body.notes) {
            const updatedNotes = req.body.notes;
            const currentNotes = tag.notes;

            const addedNotes = updatedNotes.filter((note) => !currentNotes.includes(note));
            const removedNotes = currentNotes.filter((note) => !updatedNotes.includes(note));

            if (addedNotes.length > 0) {
                await Note.updateMany(
                {
                    _id: {
                    $in: addedNotes,
                    },
                },
                {
                    $addToSet: {
                    tags: id,
                    },
                }
                );
            };

            if (removedNotes.length > 0) {
                await Note.updateMany(
                {
                    _id: {
                    $in: removedNotes,
                    },
                },
                {
                    $pull: {
                    tags: id,
                    },
                }
                );
            };
        };

        const updatedTag = await Tag.findOneAndUpdate(
            { _id: id },
            {
                $set: {
                    name: req.body.name,
                    notes: req.body.notes
                }
            },
            { new: true }
        );

        updatedTag.id = updatedTag._id;
        delete updatedTag._id;

        res.json({
            message: `Updated tag, id: ${tag._id}`,
            note: updatedTag
        });
    } catch (err) {
        console.log(err);
        res.status(500).json({
            message: 'Problem to update a tag'
        });
    };
};