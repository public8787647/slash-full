import jwt from 'jsonwebtoken';
import bcrypt, { compareSync } from 'bcrypt';
import UserScheme from '../models/user.db.js';

const SECRET = 'SECRET'; //?

export const register = async (req, res) => {
    try {    
        const password = req.body.pwdHash;
        const salt = await bcrypt.genSalt(10);
        const passwordHash = await bcrypt.hash(password, salt);
    
        const mongoDoc = new UserScheme({
            name: req.body.name,
            email: req.body.email,
            pwdHash: passwordHash
        });

        const user = await mongoDoc.save();
        
        const token = jwt.sign(
            {
                _id: user._id,
            },
            SECRET,
            {
                expiresIn: '30d'
            }
        );
    
        res.json({
            token
        });

    } catch (err) {
        console.log(err);
        res.status(500).json({
            message: 'Problem to register'
        });
    }
};

export const login = async (req, res) => {
    try {
        const user = await UserScheme.findOne({ email: req.body.email });

        if (!user) {
            throw new Error('No user found');
        }

        const isValidPwd = await bcrypt.compare(req.body.pwdHash, user._doc.pwdHash);

        if (!isValidPwd) {
            res.status(400).json({
                message: 'Wrong password or email'
            })
        }

        const token = jwt.sign(
            {
                _id: user._id,
            },
            SECRET,
            {
                expiresIn: '30d'
            }
        );

        res.json({
            token
        });

    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Problem to login'
        });
    }
};

export const userInfo = async (req, res) => {
    try {
        const user = await UserScheme.findById(req.userId);

        if (!user) {
            return res.status(404).json({
                message: 'No user found by id'
            });
        }

        const { pwdHash, ...userData } = user._doc;

        res.json({
            ...userData
        });

    } catch(error) {
        console.log(error);
        res.status(500).json({
            message: 'Problem to view user info'
        });
    }
};