import express from "express";
import cors from "cors";
import mongoose, { mongo } from 'mongoose';
import * as config from "./configs/index.js";

import { registerValid, loginValid } from './utils/auth.valid.js';
import { noteCreateValid, tagCreateValid } from './utils/note.valid.js';
import { tokenValid } from './utils/token.valid.js';
import { traceErrorValid } from './utils/error.valid.js';

import * as NoteController from "./controllers/note.controller.js";
import * as TagController from "./controllers/tag.controller.js";
import * as UserController from "./controllers/user.controller.js";

mongoose
.connect(config.DB_CONFIG.url)
.then(() => console.log('DB OK'))
.catch((err) => console.log('DB Error', err));


const app = express();

app.use(express.json());
app.use(cors(config.CORS_CONFIG));

app.get("/", (req, res) => {
    res.send("Hello");
});

// ================================================
// Routes
// ================================================

// Auth

app.post('/auth/register', registerValid, traceErrorValid, UserController.register);
app.post('/auth/login', loginValid, traceErrorValid, UserController.login);
app.get('/auth/me', tokenValid, UserController.userInfo);

// Notes

app.get('/notes', tokenValid, traceErrorValid, NoteController.getNotes);
app.get('/notes/:id', tokenValid, traceErrorValid, NoteController.getNote);
app.post('/notes', tokenValid, noteCreateValid, traceErrorValid, NoteController.createNote);
app.delete('/notes/:id', tokenValid, traceErrorValid, NoteController.deleteNote);
app.patch('/notes/:id', tokenValid, traceErrorValid, NoteController.updateNote);

// Tags

app.get('/tags', tokenValid, traceErrorValid, TagController.getTags);
app.get('/tags/:id', tokenValid, traceErrorValid, TagController.getTag);
app.post('/tags', tokenValid, traceErrorValid, TagController.createTag);
app.delete('/tags/:id', tokenValid, traceErrorValid, TagController.deleteTag);
app.patch('/tags/:id', tokenValid, traceErrorValid, TagController.updateTag);

app.listen(config.PORT, (error) => {
    if (error) {
        console.log(error);
    }
    console.log('Server OK');
});