import mongoose from "mongoose";

const NoteScheme = new mongoose.Schema(
    {
        text: {
            type: String,
        },
        tags: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Tag',
        }],
        edited: String,
        created: {
            type: String,
            required: true,
        },
        bin: {
            type: Boolean,
            default: false
        },
        author: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
            required: true,
        }
    },
    {
        timestamps: true,
    },
);

export default mongoose.model('Note', NoteScheme);