import mongoose from "mongoose";

const TagScheme = new mongoose.Schema(
    {
        name: {
            type: String,
            required: true,
        },
        notes: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Note', // Reference to the Note model
        }],
        created: {
            type: String,
            required: true,
        },
        author: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
            required: true,
        }
    },
    {
        timestamps: true,
    },
);

export default mongoose.model('Tag', TagScheme);