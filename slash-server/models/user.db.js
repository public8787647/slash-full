import mongoose from "mongoose";

const UserScheme = new mongoose.Schema(
    {
        name: {
            type: String,
            required: true,
        },
        email: {
            type: String,
            required: true,
            unique: true,
        },
        pwdHash: {
            type: String,
            required: true,
        },
        tags: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Tag',
        }],
        notes: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Note',
        }],
    },
    {
        timestamps: true,
    },
);

export default mongoose.model('User', UserScheme);