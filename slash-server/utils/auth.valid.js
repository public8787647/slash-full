import { body } from 'express-validator';

export const registerValid = [
    body('name', 'Min username length is 5 symbols').isLength({ min: 5 }),
    body('email', 'Enter a real email').isEmail(),
    body('pwdHash', 'Min password length is 5 symbols').isLength({ min: 5 })
];

export const loginValid = [
    body('email', 'Enter a real email').isEmail()
];