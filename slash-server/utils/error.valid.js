import { validationResult } from 'express-validator';

export const traceErrorValid = (req, res, next) => {
    const validErr = validationResult(req);
    if (!validErr.isEmpty()) {
        const msgArray = [];
        validErr.array().map(val => msgArray.push(val.msg))
        return res.status(400).json(msgArray);
    }
    next();
};