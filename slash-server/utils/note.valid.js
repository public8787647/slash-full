import { body } from 'express-validator';

export const noteCreateValid = [
    body('tags', 'Represent tags as array').optional().isArray(),
    body('edited', 'Date should be String').optional().isString(),
    body('created', 'Date should be String').optional().isString(),
];

export const tagCreateValid = [
    body('name', 'Min tag name length is 5 symbols').isLength({ min: 5 }).isString(),
    body('notes', 'Represent notes as array').optional().isArray(),
    body('created', 'Date should be String').optional().isString(),
];