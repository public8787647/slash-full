import jwt from 'jsonwebtoken';

const SECRET = 'SECRET'; //?

export const tokenValid = (req, res, next) => {
    const token = (req.headers.authorization || '').replace(/Bearer\s?/, '');

    if (token) {
        try {
            const decoded = jwt.verify(token, SECRET);
            req.userId = decoded._id;
    
            next();
        } catch (err) {
            return res.status(403).json({
                message: 'No access',
                error: err.message
            });
        }
    } else {
        return res.status(403).json({
            message: 'No access'
        });
    }
};